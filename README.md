# Meetup Agosto 2019

## Palestras

[Abertura](https://devopspbs.gitlab.io/meetups/meetup-2019-08-31/)

Ecosistema google e o cloud plataforma - Thiago Almeida

Como o Django mudou minha vida - Valmich Rocha


> As apresentações estão disponíveis no diretório `presentations`.

## License

GPLv3 or later.
