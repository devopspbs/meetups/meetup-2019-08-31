# DevOpsPBS
# Meetup

31/08/2019

----  ----

## O Que Temos Para Hoje

* Abertura
* Ecosistema google e o cloud plataforma - Thiago Almeida
* Como o Django mudou minha vida - Valmich Rocha

----  ----

<a href="https://www.meetup.com/devopspbs/"><img width="240" src="images/DEVOLPBS.png" alt="DevOpsPBS"></a>

Grupo de Entusiastas, Estudantes e Profissionais de TI de Parauapebas (Desenvolvedores, Sysadmins, Infosecs, QAs, Engs/Admins de rede, Técnicos de informática, Analistas, Web Designers, etc)

----  ----

## Conduta

"Participe de maneira autêntica e ativa. Ao fazer isso, você contribui para a saúde e a longevidade dessa comunidade."

[//]: # (https://garoa.net.br/wiki/C%C3%B3digo_de_Conduta_Completo)

----  ----

## Site e Mídias Sociais

* **Site oficial:** [devopspbs.org](https://devopspbs.org)

* **Mídias sociais:**
  - [meetup.com/devopspbs](https://www.meetup.com/devopspbs/)
  - [gitlab.com/devopspbs](https://gitlab.com/devopspbs)
  - [instagram/devopspbs](https://www.instagram.com/devopspbs/)

* **Grupos de discussão:**
  - Grupo [DevOpsPBS](https://t.me/joinchat/A-G57xd_fjAnQwOzV_sdeQ) no Telegram
  - Grupo [TECNOLOGIA PBS](https://chat.whatsapp.com/7XfTiu53icSKKbANTQnMGH) no Whatsapp

----  ----

## Apoio:

<a href="http://www.parauapebas.pa.gov.br/"><img height="200" src="images/parauapebas.svg" alt="Parauapebas"></a>

----  ----

## Dica: Linkedin Mobile

![](images/linkedin.png)
